function addQ(text) {
    const div = document.createElement("div")
    div.textContent = text
    document.querySelector("body").appendChild(div)
}

function addKata(stuff) {
    const div = document.createElement("div")
    div.textContent = stuff
    document.querySelector("body").appendChild(div)
}

//print numbers from 1 to 20
addQ("1.) print numbers from 1 to 20")
for (let i=0; i<20; i++)
{
	addKata(i+1);
}

//display the even numbers from 1 to 20
addQ("2.) display the even numbers from 1 to 20")
for(let i=0; i<=20; i+=2)
{
	addKata(i);
}

//display the odd numbers from 1 to 20
addQ("3.) display the odd numbers from 1 to 20")
for (let t=1; t<20; t= t+2)
{
	addKata(t);
}

//display the multiples of 5 up to 100
addQ("4.) display the multiples of 5 up to 100")
let t=0;
while(t<=100)
{
	if (t % 5 === 0)
	{
        addKata(t);
        t++
	}
	
	else
	{
		t++;
	}
}

//display the square numbers from 1 to 100
addQ("5.) display the square numbers from 1 to 100")
for(t=0; t<10; t++)
{
	sqr = Math.pow (t, 2);
	addKata(sqr);
}

//display the numbers countin backwards from 20 to 1
addQ("6.) display the numbers countin backwards from 20 to 1")
for(t=20; t>1; t--)
{
	addKata(t);
}

//display the even numbers counting backwards from 20 to 1
addQ("7.) display the even numbers counting backwards from 20 to 1")
for(t=20; t>1; t= t - 2)
{
	addKata(t);
}

//display the odd numbers counting backwards from 20 to 1
addQ("8.) display the odd numbers counting backwards from 20 to 1")
for(t=19; t>1; t=t-2)
{
	addKata(t)
}

//display the multiples of five counting backwards from 100
addQ("9.) display the multiples of five counting backwards from 100")
let ta=100;
while(ta>4)
{
	if (ta % 5 ===0)
	{
        addKata(ta);
        ta--
	}
	
	else
	{
		ta--;
	}
}

//display the square numbers counting down from 100
addQ("10.) display the square numbers counting down from 100")
for(t=10; t>1; t--)
{
	sqr = Math.pow (t, 2);
	addKata(sqr);
}

//display the sample array
addQ("11.) display the sample array")
let sample_array = [469, 755, 244, 245, 750, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472]

for(let i = 0; i < sample_array.length; i++)
{
    let count = sample_array[i];
    addKata(count);
    
}

//display all the even numbers in the sample array
addQ("12.) display all the even numbers in the sample array")
let i = 0;
while (i < sample_array.length)
{
    let count = sample_array[i];

    if(count % 2 === 0)
    {
        addKata(count);
        i++;
    }

    else
    {
        i++;
    }
}

//display all the odd numbers in the sample array
addQ("13.) display all the odd numbers in the sample array")
let ia = 0;
while (ia < sample_array.length)
{
    let count = sample_array[ia];

    if(count % 2 !== 0)
    {
        addKata(count);
        ia++;
    }

    else
    {
        ia++;
    }
}

//display the square of each value of the array
addQ("14.) display the square of each value of the array")
for(let ib = 0; ib < sample_array.length; ib++)
{
    let count = sample_array[ib];
    sqr = Math.pow (count, 2);
    addKata(sqr);
}

//display the sum of all the numbers from 1 to 20
addQ("15.) display the sum of all the numbers from 1 to 20")
let sum = 0;

for(let i = 0; i < 21; i++)
{
    let count = i;
    sum = sum + count;
}

addKata(sum);

//display the sum of all the elements in sample array
addQ("16.) display the sum of all the elements in sample array")
sum = 0;

for(let i = 0; i < sample_array.length; i++)
{
    let count = sample_array[i];
    sum = sum + count;
}

addKata(sum);

//display the smallest element in the sample array
addQ("17.) display the smallest element in the sample array")
let comp = sample_array[0];

for(let i = 1; i < sample_array.length; i++)
{
    let count = sample_array[i];
    if (comp > count)
    {
        comp = count;
    }

    else
    {
        continue;
    }

}

addKata(comp);

//display the largest element in the sample array
addQ("18.) display the largest element in the sample array")
comp = sample_array[0];

for(let i = 1; i < sample_array.length; i++)
{
    let count = sample_array[i];
    if (comp < count)
    {
        comp = count;
    }

    else
    {
        continue;
    }

}

addKata(comp);



